#pragma once

#ifdef _DEBUG
    #pragma comment(lib, "HShield-5.4.8.1-MTd.lib")
#else
    #pragma comment(lib, "HShield-5.4.8.1-MT.lib")
#endif

#pragma comment(lib, "HSUserUtil-5.4.8.1-MT.lib")

