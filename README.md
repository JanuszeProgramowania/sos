Schemat nazywania: 1.0.kolejny numer wersji.game_internaldev.data w formacie YYMMDD-HHMM

1.0.0001.game_internaldev.140622-1429 - Powstanie repozytorium. Zmiana algorytmu treningu kamieniem duchowym na szanse procentowe (char_skill.cpp:325)

1.0.0002.game_internaldev.140626-1150 - zdjecie limitu expa za moba(max 10% do lvla)[char_battle.cpp:2487] i wylaczanie klienta, gdy nie zgadzaja sie pakiety[input_login.cpp:1117]

1.0.0003.game_internaldev.140628-0930 - zmiana przedmiotów otrzymywanych za wbicie kulki i poziomu [char.cpp:3202] i dodanie funkcji questowych, za pomocą których można dodawać i zmieniać istniejące bonusy przedmiotu [questlua_item.cpp:139, 150, 161, 172]

1.0.0004.game_internaldev.140628-1408 - mozliwosc zmiany zalozonych itemow majac pelny ekwipunek[char_item.cpp:6012-6015]

1.0.0005.game_internaldev.140701-2222 - zmiana przedmiotów otrzymywanych za wbicie kulki i poziomu poniżej 10 poziomu z małych potek na średnie potki[char.cpp:3206]

1.0.0006.game_internaldev.140701-2253 - zmiana szansy procentowej na wejście kamienia duchowego[char_skill.cpp:325]

1.0.0007.avoris_alpha.140720-1030 - fix buga na stackowanie hp[char_item.cpp:4661-4664]

1.0.0008.avoris_alpha.140720-1904 - poprawka czasu rezerwacji [db/src/GuildManager.cpp:42] i rozpoczęcia wojny gildii [db/src/GuildManager.cpp:33]

1.0.0009.avoris_alpha.140722-1738 - fix#2 buga na stackowanie hp [char_item:4504-4506]

1.0.0010.avoris_alpha.140729-1143 - globalny chat dla wszystkich królestw

1.0.0011.avoris_alpha.140729-1418 - nowe parametry dla komendy eclipse (questmanager.cpp:1248)

1.0.0012.avoris_alpha.140730-1128 - blokada drophacka[char_item.cpp:5905-5906]

1.0.0013.avoris_alpha.140730-1238 - zmiana przedmiotów otrzymywanych za wbicie kulki i poziomu [char.cpp:3202]

1.0.0014.avoris_alpha.140731-1138 - blokada buga na pt(do testowania)[party.cpp:403-405]

1.0.0015.avoris_alpha.140731-1318 - wycofanie zmiany z poprzedniego commita. zmiana przedmiotów dawanych komendą /item_full_set [cmd_gm.cpp:4112]

1.0.0016.avoris_alpha.140731-1651 - blokada buga z pt wywalającego channele(tym razem powinno działać - mi nie udało się zbugować)[party.cpp:446-449]

1.0.0017.avoris_alpha.140801-1444 - zdjęcie podatku [shop.cpp:320] [shop_manager.cpp:318]

1.0.0018.avoris_alpha.140801-1455 - próba dodania zmiany eclipse w poleceniach bash_panelu [input.cpp:604-608]

1.0.0019.avoris_alpha.140803-1333 - punkty statystyk powinny być już przyznawane powyżej 91 poziomu [char.cpp:3173]

1.0.0020.avoris_alpha.140804-1617 - fix buga na niewidzialność[char.cpp:4837-4838]

1.0.0021.avoris_alpha.140804-1723 - wyłączenie potrójnych cen w innym królestwie[shop.cpp:238-239] i test pętli do słowofiltrów[input_main.cpp:697-701]

1.0.0022.avoris_alpha.140805-1240 - zmiana szansy na wejście bonusu [constants.cpp:932]

1.0.0023.avoris_alpha.140805-1249 - zmiana min ilości członków gildii do rozpoczęcia wojny [guild.h:19]

1.0.0024.avoris_alpha.140805-1340 - zmiana #2 min ilości członków gildii do rozpoczęcia wojny [guild.h:19]

1.0.0025.avoris_alpha.140805-1425 - fix buga na niewidzialność[char.cpp:4837-4838][char_skill.cpp:2422]

1.0.0026.avoris_prebeta.140808-1455 - fix buga z odejmowaniem HP danego przez affect po użyciu szarży[char_skil:2399]

1.0.0027.avoris_prebeta.140810-1215 - punkty statystyk będą już przyznawane powyżej 91 poziomu [char.cpp:3173]

1.0.0028.avoris_prebeta.140816-1025 - dodanie obsługi anty exp ringa [char_battle.cpp:2336-2337] [unique_item.h:33]

1.0.0029.avoris_prebeta.140816-1130 - poprawka ECLIPSE dla bash_panelu [input.cpp:604-612]

1.0.0030.avoris_prebeta.140817-2114 - próba wprowadzenia słowofiltrów [input_main.cpp:695-705]

1.0.0031.avoris_prebeta.140817-2232 - usunięcie słowofiltrów[input_main.cpp:695-705]

1.0.0032.avoris_prebeta.140819-1248 - dodanie komendy /auth [cmd.cpp:76,594] [cmd_general.cpp:2500-2507]

1.0.0033.avoris_prebeta.140819-1255 - zmiana metody wyświetlania informacji o autorach za pomocą komendy /auth [cmd_general.cpp:2500-2507]

1.0.0034.avoris_prebeta.140824-1011 - zmiana id anty exp ringa [unique_item.h:33]

1.0.0035.avoris_prebeta.140824-1317 - test fixa na cofające hp[char.cpp:2379-2384->2394-2405] i poprawka literówki w systemie monarchów[db/src/Monarch.cpp:166]

1.0.0036.avoris_prebeta.140824-1404 - fix buga z kamieniami duchowymi[char_item.cpp:3493-3494][char_item.cpp:4449-4450]

1.0.0037.avoris_prebeta.140903-0754 - próba dodania efektu użycia mikstury do zielonych potek[char_item.cpp:1801,2197]

1.0.0038.avoris_prebeta.140903-1103 - test fixa na potrójną szarżę[char_skill.cpp:2399] i fixy na system pasów(możliwość wkładania wszystkich itemów za pośrednictwem dozorcy)input_main.cpp:45,2099-2104[char_item.cpp:6027-6028]

1.0.0039.avoris_prebeta.140903-1149 - fix potrójnej szarży(tym razem działający)[char_skill:2399]

1.0.0040.avoris_prebeta.140920-1702 - fix timebomby[desc.cpp:202-210]

1.0.0041.avoris-prebeta.140920-1739 - resztki fixu[main.cpp:252-257, 641-646][db/src/ClientManagerLogin.cpp:84-100]

1.0.0042.avoris_prebeta.141018-1026 - ochrona sklepów(ustawianie pkmode przy otwarciu sklepu na protect) [char.cpp:743,764]

1.0.0043.avoris_prebeta.141019-1216 - zabezpieczenie przed handlem z GM[exchange.cpp:54-58; input_main.cpp:1208-1209]

1.0.0044.avoris_prebeta.141019-1310 - bonusy dla gildii, która wygrała wojnę [db/src/GuildManager.cpp:355]

1.0.0045.avoris_prebeta.141019-1315 - poprawka do bonusów gildii [db/src/GuildManager.cpp:355]

1.0.0046.avoris-prebeta.141215-2247 - fix bind_ip[config.cpp:56,291-299,343,1017], blokada handlu z GM

1.0.0047.avoris-prebeta.141216-2254 - rozne fixy[http://www.elitepvpers.com/forum/metin2-pserver-guides-strategies/3498686-c-einige-n-tzliche-erweiterungen.html]

1.0.0048.avoris-prebeta.141216-2310 - fix na podwojne zliczanie killi(teraz powinno byc okej)

1.0.0049.avoris-prebeta.150104-0934 - próba dodania efektu użycia mikstury do zielonych i fioletowych potek [char_item.cpp:1801;2194;2485;4307;4312]

1.0.0050.avoris-prebeta.160419-1215 - przystosowanie do kompilacji pod Windowsem z użyciem toolsetu vc120